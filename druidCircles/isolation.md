## Circle of Isolation

#### Circle of Isolation Features
| Druid Level | Features                       |
|:-----------:|:-------------------------------|
|     2nd     |  Woodcraft, Wild Retribution   |
|     6th     | Protector of the Natural Order |
|     10th    |                                |
|     14th    |                                |

### Naturally Crafty
*2nd-level Isolation feature*

You gain proficiency in simple weapons, and know how to make non-metal equivalents of them from natural materials. Over the course of a short rest, you may create one simple weapon, or 10 pieces of ammunition.

### Wild Retribution
*2nd-level Isolation feature*

You can call upon the lifeforce of Nature to fight those who would harm it. Summoning up the essence of plants and animals recently slain, you can use them to augment your weapon attacks. You have a pool of this essence represented by a number of d10s equal to your druid level.

When you roll damage for a weapon attack, you may augment that damage by spending dice from the pool. You may spend a number of dice equal to your proficiency bonus or less. Roll the spent dice and add them to your damage as acid damage.

You regain the expended dice when you finish a long rest.

### Protector of the Natural Order
*6th-level Isolation feature*

You've been trained to protect the wild places from all those who would subsume them. When making a weapon attack against a humanoid, undead, or fiend, you may add your wisdom modifier to the attack roll.

Additionally, if you add your Wild Retribution dice to an attack made in beast form, said attack counts as magical for the purpose of overcoming resistance and immunity to nonmagical attacks and damage.
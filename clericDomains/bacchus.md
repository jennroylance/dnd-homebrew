## Domain of Bacchus

God of Wine, Revelry, and Frenzy

#### Domain of Bacchus Features

| Cleric Level | Features                                            |
| :----------: | :-------------------------------------------------- |
|     1st      | Domain Spells, Bonus Cantrip, Drunken Functionality |
|     2nd      | Channel Divinity: _[Name]_                          |
|     6th      | [Features]                                          |
|     8th      | Potent Spellcasting                                 |
|     17th     | [Features]                                          |

##### Domain of Bacchus Spells

| Cleric Level | Spells                                |
| :----------: | :------------------------------------ |
|     1st      | _Charm Person, Entangle_              |
|     3rd      | _Spike Growth, Crown of Madness_      |
|     5th      | _Create Food and Water, Plant Growth_ |
|     7th      | _Confusion, Polymorph_                |
|     9th      | _Geas, Synaptic Static_               |

### Bonus Cantrip

_1st-level Bacchus feature_

You gain the _Friends_ cantrip if you don't already know it. For you, this cantrip counts as a cleric cantrip.

### Drunken Functionality

_1st-level Bacchus feature_

Clerics of Bacchus have resistance to poison and advantage on saves against the poisoned condition. Furthermore, drunkenness has no effect on your attack rolls or ability checks.

### Channel Divinity: _[Name]_

_2nd-level Bacchus feature_

You gain this additional Channel Divinity option.

### [6th Level Features]

_6th-level Bacchus feature_

### Potent Spellcasting

_8th-level Bacchus feature_

You add your Wisdom modifier to the damage you deal with any cleric cantrip.

### [17th Level Features]

_17th-level Bacchus feature_

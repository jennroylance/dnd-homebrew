## Domain of Vesta

Goddess of Hearth and Home

##### Domain of Vesta Features

| Cleric Level | Features                            |
| :----------: | :---------------------------------- |
|     1st      | Blessing of the Meek, Domain Spells |
|     2nd      | Channel Divinity: _Heart's Fire_    |
|     6th      |                                     |
|     8th      | Potent Spellcasting                 |
|     17th     |                                     |

##### Domain of Vesta Spells

| Cleric Level | Spells                               |
| :----------: | :----------------------------------- |
|     1st      | _Purify Food and Drink, Goodberry_   |
|     3rd      | _Continual Flame, Prayer of Healing_ |
|     5th      | _Create Food and Water, Tiny Hut_    |
|     7th      | _Fire Shield, Aura of Life_          |
|     9th      | _Greater Restoration, Hallow_        |

### Blessing of the Meek

_1st-level Vesta feature_

While not wearing armor or wielding any weapons, your AC equals 10 + your Wisdom modifier + your Constitution Modifier. You can use a shield and still gain this benefit.

### Channel Divinity: _Heart's Fire_

_2nd-level Vesta feature_

You gain this additional Channel Divinity option. Invoking the power of the Hearth, you kindle a cheery fire, which lasts 8 hours, in an available space within 30 feet. Any creature who spends at least 10 minutes within 5 feet of the fire gains temporary hit points equal to your wisdom modifier. Furthermore, any creature who spends a short rest within 20 feet of the fire and expends hit dice to recover their health can treat the lowest roll of their hit dice as if it had rolled it's maximum value.

### [6th Level Features]

_6th-level Vesta feature_

### Potent Spellcasting

_8th-level Vesta feature_

You add your Wisdom modifier to the damage you deal with any cleric cantrip.

### [17th Level Features]

_17th-level Vesta feature_

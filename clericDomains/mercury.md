## Domain of Mercury

God of Travelers, Messages, Thieves, and Luck

#### Domain of Mercury Features

| Cleric Level | Features                                    |
| :----------: | :------------------------------------------ |
|     1st      | Domain Spells, Bonus Cantrip, Fleet of Foot |
|     2nd      | Channel Divinity: _Escape Route_            |
|     6th      | Swift Cantrip                               |
|     8th      | Divine Strike                               |
|     17th     | Improved Swift Cantrip                      |

##### Domain of Mercury Spells

| Cleric Level | Spells                                    |
| :----------: | :---------------------------------------- |
|     1st      | _Longstrider, Zephyr Strike_              |
|     3rd      | _Pass Without Trace, Spider Climb_        |
|     5th      | _Thunder Step, Blink_                     |
|     7th      | _Freedom of Movement, Secret Chest_       |
|     9th      | _Steel Wind Strike, Teleportation Circle_ |

### Bonus Cantrips

_1st-level Mercury feature_

Upon taking this subclass, you gain the _Booming Blade_ and _Message_ cantrips if you don't already know them. For you, these cantrips counts as cleric cantrips.

### Good Fortune

_1st-level Mercury feature_

When you fail a saving throw or miss an attack roll, you may roll a d4 and add it to the total, possibly changing the outcome. You may use this feature a number of times equal to your proficiency bonus and regain all uses on a long rest.

### Channel Divinity: _Escape Route_

_2nd-level Mercury feature_

You gain this additional Channel Divinity option. As a bonus action, you can verbally call upon divine power to escape your enemies. When you do so, you can teleport up to 30 feet to an unoccupied space that you can see.

### Swift Cantrip

_6th-level Mercury feature_

You can use a weapon with the _light_ property as a spellcasting focus for your cleric spells. Additionally, if you use your action to cast a cleric cantrip while dual wielding, you can make an offhand attack as a bonus action.

### Divine Strike

_8th-level Mercury feature_

You gain the ability to infuse your weapon strikes with divine energy. Once on each of your turns when you hit a creature with a weapon attack, you can cause the attack to deal an extra 1d8 radiant damage to the target. When you reach 14th level, the extra damage increases to 2d8.

### Luck of the Gods

_17th-level Mercury feature_

Add _Glibness_ and _Foresight_ to your list of domain spells.Like your other domain spells, they are always prepared and count as cleric spells for you.

## Domain of Minerva

Goddess of Wisdom, Law, Defense, and Strategic Warfare

#### Domain of Minerva Features

| Cleric Level | Features                                             |
| :----------: | :--------------------------------------------------- |
|     1st      | Domain Spells, Bonus Proficiencies, Mind Over Matter |
|     2nd      | Channel Divinity: _Strategic Insight_                |
|     6th      | Press the Advantage                                  |
|     8th      | Divine Strike                                        |
|     17th     | Fortune Favors the Prepared                          |

##### Domain of Minerva Spells

| Cleric Level | Spells                                |
| :----------: | :------------------------------------ |
|     1st      | _Sanctuary, Compelled Duel_           |
|     3rd      | _Hold Person, Heat Metal_             |
|     5th      | _Blinding Smite, Elemental Weapon_    |
|     7th      | _Freedom of Movement, Elemental Bane_ |
|     9th      | _Hold Monster, Banishing Smite_       |

### Bonus Proficiencies

_1st-level Minerva feature_

You gain proficiency with martial weapons and heavy armor.

### Mind Over Matter

_1st-level Minerva feature_

Having been trained in the arts of war, you understand that more important than strength is how you use it. When you attack with a weapon you are proficient with, you can use your Wisdom modifier, instead of Strength or Dexterity, for the attack and damage rolls. You can use this ability a number of times equal to your cleric level, and regain all uses upon the completion of a short or long rest.

### Channel Divinity: _Strategic Insight_

_2nd-level Minerva feature_

You gain this additional Channel Divinity option. You call upon divine aid (no action required), and are given understanding of how to turn your situation to your benefit. Your next weapon attack this turn is made with advantage.

### Press the Advantage

_6th-level Minerva feature_

When you make a weapon attack with advantage, if it hits you may make an additional weapon attack as a bonus action.

### Divine Strike

_8th-level Minerva feature_

You gain the ability to infuse your weapon strikes with divine energy. Once on each of your turns when you hit a creature with a weapon attack, you can cause the attack to deal an extra 1d8 damage of the same type dealt by the weapon to the target. When you reach 14th level, the extra damage increases to 2d8.

### Fortune Favors the Prepared

_17th-level Minerva feature_

At 17th level, when you must make a saving throw, you may gain a bonus to the saving throw equal to your Wisdom modifier (minimum bonus of +1). You may use this ability a number of times equal to your Wisdom modifier and regain all uses on a long rest.
